#ifndef OPERATE_H_H_H
#define OPERATE_H_H_H

void InitAllVariables();
POINT GenerateNewApple();
void SetGameStart(HWND hwnd, BOOL bIsSetStart);
void SetGameOver(HWND hwnd);
void MoveOneStep();
BOOL IsStrikeApple();
void AddSnakeLength();
BOOL IsStrikeWallOrSelf();

#endif