#ifndef DRAWANDSHOW_H_H_H
#define DRAWANDSHOW_H_H_H

void DrawWorkGrid(HDC hdc);
void DrawSquare(HDC hdc, const POINT pt);
void DrawSnake(HDC hdc, const BOOL bIsClearSnake);
void DrawApple(HDC hdc);

#endif