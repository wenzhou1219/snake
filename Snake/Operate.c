#include <Windows.h>
#include "Operate.h"
#include "Variable.h"

/************************************************************************/
/*功能：初始化所有变量
**@20130527 by 文洲*/
/************************************************************************/
void InitAllVariables()
{
	int i;

	bIsGameStart = FALSE;							//游戏初始状态为暂停
	ptApplePos = GenerateNewApple();				//随机产生苹果的位置

	//最开始蛇身长为1，初始位置为工作区中间
	uiSnakeLength = 1;
	ptSnakePos[0].x = WORK_WINDOW_WIDTH/2;
	ptSnakePos[0].y = WORK_WINDOW_HEIGHT/2;
	for (i=1; i<WORK_WINDOW_AERA; i++)
	{
		ptSnakePos[i].x = -1;//-1表示该位置不是蛇身
		ptSnakePos[i].y = -1;
	}

	dirNextMove = UP;								//最开始默认运动方向为向上
	uiTimerElapse = 500;							//运动的间隔时间默认为500ms
}

/************************************************************************/
/*功能：随机产生下一个苹果的位置
**@20130527 by 文洲*/
/************************************************************************/
POINT GenerateNewApple()
{
	POINT ptTemp;
	BOOL bFlag = FALSE;
	int i;

	do 
	{
		ptTemp.x = rand()%(WORK_WINDOW_WIDTH);
		ptTemp.y = rand()%(WORK_WINDOW_HEIGHT);

		//确保产生的苹果不会覆盖在现在蛇身的位置上
		for (i=0; i<uiSnakeLength; i++)
		{
			if (ptTemp.x==ptSnakePos[i].x && ptTemp.y==ptSnakePos[i].y)
			{
				bFlag = TRUE;//还要重新产生苹果
				break;
			}
		}

		if (i == uiSnakeLength)
		{
			bFlag = FALSE;//不用重新产生苹果
		}
	} while (bFlag == TRUE);

	return ptTemp;
}

/************************************************************************/
/*功能：设置游戏开始或暂停
**@20130527 by 文洲*/
/************************************************************************/
void SetGameStart(HWND hwnd, BOOL bIsSetStart)
{
	if (bIsGameStart != bIsSetStart)
	{
		if (bIsSetStart == TRUE)
		{
			SetTimer(hwnd, TIMER_1, uiTimerElapse, NULL);
			bIsGameStart = TRUE;
		}
		else
		{
			KillTimer(hwnd, TIMER_1);
			bIsGameStart = FALSE;
		}
	}
}

/************************************************************************/
/*功能：设置游戏结束
**@20130527 by 文洲*/
/************************************************************************/
void SetGameOver(HWND hwnd)
{
	if (bIsGameStart == TRUE)
	{
		KillTimer(hwnd, TIMER_1);
	}
}

/************************************************************************/
/*功能：向指定的方向运动一个单位
**@20130527 by 文洲*/
/************************************************************************/
void MoveOneStep()
{
	int i;

	//每个蛇尾位置为前一个靠近蛇头的位置
	for (i=0; i<uiSnakeLength-1; i++)
	{
		ptSnakePos[i] = ptSnakePos[i+1];
	}

	//蛇头向指定方向运动一个单位
	switch (dirNextMove)
	{
	case UP:
		ptSnakePos[uiSnakeLength-1].y -=1;
		break;
	case DOWN:
		ptSnakePos[uiSnakeLength-1].y +=1;
		break;
	case LEFT:
		ptSnakePos[uiSnakeLength-1].x -=1;
		break;
	case RIGHT:
		ptSnakePos[uiSnakeLength-1].x +=1;
		break;
	}
}

/************************************************************************/
/*功能：判断蛇是否撞上苹果
**@20130527 by 文洲*/
/************************************************************************/
BOOL IsStrikeApple()
{
	POINT ptTemp;

	//求假设蛇向指定方向运动一个单位的蛇头位置
	ptTemp = ptSnakePos[uiSnakeLength-1];
	switch (dirNextMove)
	{
	case UP:
		ptTemp.y -= 1;
		break;
	case DOWN:
		ptTemp.y += 1;
		break;
	case LEFT:
		ptTemp.x -= 1;
		break;
	case RIGHT:
		ptTemp.x += 1;
		break;
	}

	//求取的假设蛇头位置和苹果位置相同则相撞
	return(ptTemp.x==ptApplePos.x && ptTemp.y==ptApplePos.y);
}

/************************************************************************/
/*功能：当前苹果位置设为新蛇头，蛇长度加1
**@20130527 by 文洲*/
/************************************************************************/
void AddSnakeLength()
{
	ptSnakePos[uiSnakeLength++] = ptApplePos;
}

/************************************************************************/
/*功能：判断蛇是否撞上墙或自身
**@20130527 by 文洲*/
/************************************************************************/
BOOL IsStrikeWallOrSelf()
{
	POINT ptTemp;
	int i;

	//求假设蛇向指定方向运动一个单位的蛇头位置
	ptTemp = ptSnakePos[uiSnakeLength-1];
	switch (dirNextMove)
	{
	case UP:
		ptTemp.y -= 1;
		break;
	case DOWN:
		ptTemp.y += 1;
		break;
	case LEFT:
		ptTemp.x -= 1;
		break;
	case RIGHT:
		ptTemp.x += 1;
		break;
	}

	//假设蛇头位置超出工作区则判断是撞在墙上
	if (ptTemp.x<0 || ptTemp.x>WORK_WINDOW_WIDTH-1 || ptTemp.y<0 || ptTemp.y>WORK_WINDOW_HEIGHT-1)
	{
		return TRUE;
	}

	//假设蛇头位置和已有的自身位置中的一个相同则判断是撞到自身
	for (i = 1; i < uiSnakeLength-1; i++)
	{
		if (ptTemp.x==ptSnakePos[i].x && ptTemp.y==ptSnakePos[i].y)
		{
			return TRUE;
		}
	}

	//没有撞到墙或自身
	return FALSE;
}