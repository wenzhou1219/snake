//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Snake.rc
//
#define IDR_MAINMENU                    101
#define IDI_APPICON                     103
#define ID_40001                        40001
#define IDM_STARTPAUSE                  40002
#define IDM_QUIT                        40003

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        104
#define _APS_NEXT_COMMAND_VALUE         40004
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
