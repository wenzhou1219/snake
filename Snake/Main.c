#include <Windows.h>
#include "WndProc.h"
#include "resource.h"
#include "Variable.h"

int WINAPI WinMain (HINSTANCE hInstance, HINSTANCE hPrevInstance, PSTR szCmdLine, int iCmdShow)
{
	static TCHAR	szAppName[] = TEXT ("Snake");
	HWND			hwnd;
	MSG				msg;
	WNDCLASSEX		wndclassex = {0};

	//窗口类设计
	wndclassex.cbSize			= sizeof(WNDCLASSEX);
	wndclassex.style			= CS_HREDRAW | CS_VREDRAW;
	wndclassex.lpfnWndProc		= WndProc;
	wndclassex.cbClsExtra		= 0;
	wndclassex.cbWndExtra		= 0;
	wndclassex.hInstance		= hInstance;
	wndclassex.hIcon			= LoadIcon (hInstance, MAKEINTRESOURCE(IDI_APPICON));//采用自定义图标
	wndclassex.hCursor			= LoadCursor (NULL, IDC_ARROW);
	wndclassex.hbrBackground	= (HBRUSH) GetStockObject (WHITE_BRUSH);
	wndclassex.lpszMenuName		= MAKEINTRESOURCE(IDR_MAINMENU);
	wndclassex.lpszClassName	= szAppName;
	wndclassex.hIconSm			= wndclassex.hIcon;

	//注册窗口类
	if (!RegisterClassEx (&wndclassex))
	{
		MessageBox (NULL, TEXT ("RegisterClassEx failed!"), szAppName, MB_ICONERROR);
		return 0;
	}

	//创建窗口
	hwnd = CreateWindowEx (WS_EX_OVERLAPPEDWINDOW , 
		szAppName, 
		TEXT ("贪吃蛇V1"),
		WS_OVERLAPPEDWINDOW & ~WS_THICKFRAME & ~WS_MAXIMIZEBOX,//普通窗口上去掉动态调整大小和最大化功能
		CW_USEDEFAULT, 
		CW_USEDEFAULT, 
		2*GetSystemMetrics(SM_CYEDGE)+2*GetSystemMetrics(SM_CYFIXEDFRAME)+WORK_WINDOW_WIDTH*SQUARE_SIZE-(WORK_WINDOW_WIDTH-1),//注意加上3D部分SM_CYEDGE
		2*GetSystemMetrics(SM_CXEDGE)+2*GetSystemMetrics(SM_CYFIXEDFRAME)+GetSystemMetrics(SM_CYCAPTION)+GetSystemMetrics(SM_CYMENU)+WORK_WINDOW_HEIGHT*SQUARE_SIZE-(WORK_WINDOW_HEIGHT-1),
		NULL, 
		NULL, 
		hInstance,
		NULL); 

	//及时更新显示窗口
	ShowWindow (hwnd, iCmdShow);
	UpdateWindow (hwnd);

	//消息循环
	while (GetMessage (&msg, NULL, 0, 0))
	{
		TranslateMessage (&msg);
		DispatchMessage (&msg);
	}

	return msg.wParam;
}