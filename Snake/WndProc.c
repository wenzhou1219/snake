#include <Windows.h>
#include "resource.h"
#include "Variable.h"
#include "Operate.h"
#include "DrawAndShow.h"

//引入PlaySound函数的动态链接库，注意此处"winmm.lib"不能加TEXT,这是一个ASCLL函数
#pragma comment(lib, "winmm.lib")

LRESULT CALLBACK WndProc (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	HDC				hdc;
	PAINTSTRUCT		ps;
	HMENU			hMenu;

	switch (message)
	{
	case WM_CREATE:
		//设置定时器种子为系统时间
		srand(GetTickCount());

		//初始化变量
		InitAllVariables();
		return (0);

	case WM_PAINT:
		hdc = BeginPaint (hwnd, &ps);

		DrawWorkGrid(hdc);				//绘制工作区网格
 		DrawSnake(hdc, FALSE);			//绘制蛇身
 		DrawApple(hdc);					//绘制苹果

		return (0);

	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case IDM_STARTPAUSE:
			hMenu = GetMenu(hwnd);

			if (bIsGameStart == TRUE)
			{
				SetGameStart(hwnd, FALSE);
				CheckMenuItem(GetSubMenu(hMenu,0), 0, MF_BYPOSITION | MF_UNCHECKED);
			}
			else
			{
				SetGameStart(hwnd, TRUE);
				CheckMenuItem(GetSubMenu(hMenu,0), 0, MF_BYPOSITION | MF_CHECKED);
			}

			DeleteObject(hMenu);
			break;

		case IDM_QUIT:
			SendMessage(hwnd, WM_DESTROY, (LPARAM)0, (LPARAM)0);
			break;
		}
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_UP:
			if (dirNextMove != DOWN)//不能向当前运动方向的相反方向运动
			{
				dirNextMove = UP;
			}
			break;
		case VK_DOWN:
			if (dirNextMove != UP)//不能向当前运动方向的相反方向运动
			{
				dirNextMove = DOWN;
			}
			break;
		case VK_LEFT:
			if (dirNextMove != RIGHT)//不能向当前运动方向的相反方向运动
			{
				dirNextMove = LEFT;
			}
			break;
		case VK_RIGHT:
			if (dirNextMove != LEFT)//不能向当前运动方向的相反方向运动
			{
				dirNextMove = RIGHT;
			}
			break;
		case VK_SPACE:
			SendMessage(hwnd, WM_COMMAND, MAKEWPARAM(IDM_STARTPAUSE,0), (LPARAM)0);
			break;
		}
		break;

	case WM_TIMER:
		hdc = GetDC(hwnd);

		if (IsStrikeApple())					//是否撞上苹果
		{
			PlaySound(TEXT(".\\Sound\\eat.wav"), NULL, SND_FILENAME | SND_ASYNC);
			AddSnakeLength();					//当前蛇身加1
			ptApplePos = GenerateNewApple();	//产生新苹果
			DrawApple(hdc);						//绘制新苹果
		}
		else if (IsStrikeWallOrSelf())			//是否撞上墙或自身
		{
			SetGameOver(hwnd);
			MessageBeep(MB_ICONEXCLAMATION);
			MessageBox(hwnd, TEXT("游戏结束"), TEXT("游戏结束"), MB_OK);
		}
		else
		{
			PlaySound(TEXT(".\\Sound\\move.wav"), NULL, SND_FILENAME | SND_ASYNC);
			DrawSnake(hdc, TRUE);				//擦除旧蛇身
			MoveOneStep();						//按指定方向运动一个单位
			DrawSnake(hdc, FALSE);				//绘制新蛇身
		}

		ReleaseDC(hwnd, hdc);
		return (0);

	case WM_DESTROY:
		SetGameOver(hwnd);
		PostQuitMessage (0);
		return (0);
	}
	return DefWindowProc (hwnd, message, wParam, lParam);
}